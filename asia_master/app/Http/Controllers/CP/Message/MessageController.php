<?php

namespace App\Http\Controllers\CP\Message;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Message as Model;


class MessageController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.message";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

   public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('en_name', 'like', '%'.$key.'%')->orWhere('kh_name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('id','DESC')->paginate($limit);
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data,'appends'=>$appends]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }

    public function store(Request $request) {
        $slide_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        Validator::make(
            $request->all(), 
                         [
                            
                           'name'      => 'required',


                        
                        ])->validate();
        $data = array(

                    'name'        =>      $request->input('name'),
                    'email'       =>      $request->input('email'),
                    'phone'       =>      $request->input('phone'),
                    'message'     =>      $request->input('message'),
                    'creator_id'  =>      $slide_id,
                    'created_at'  =>      $now
                );
        
        Session::flash('invalidData', $data );
       
        $image = FileUpload::uploadFile($request, 'image', 'uploads/slide');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
    $id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
    return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $slide_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name'          =>      $request->input('name'),
                    'email'          =>      $request->input('email'),
                    'phone'    =>      $request->input('phone'),
                    'message'    =>      $request->input('message'),
                    'updater_id'        =>      $slide_id,
                    'updated_at'        =>      $now
                );
        

        Validator::make(
                $data, 
                [
                            
                           'name'     => 'required',
                           
                           'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
            ]);

        $image = FileUpload::uploadFile($request, 'image', 'uploads/slide');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
  }

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Message has been deleted'
        ]);
    }
    function updateStatus(Request $request){
       // return $request;
      $id   = $request->input('id');
      if($request->active != ''){
        $data = array('is_published' => $request->input('active'));
      }
      if($request->position != ''){
        $data = array('is_position' => $request->position);
      }
      if($request->logo != ''){
        $data = array('is_logo' => $request->logo);
      }
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Status has been updated.'
      ]);
    }
}
