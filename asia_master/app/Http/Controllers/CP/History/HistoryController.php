<?php

namespace App\Http\Controllers\CP\History;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Contents as Content;


class HistoryController extends Controller
{
    
  function __construct (){
    $this->route = "cp.history";
 }

 public function update(Request $request){   
   $id = $request->input('id');
   $image = "";
  
   $data = array( 
                
                 'kh_content' =>   $request->input('kh_content'),
                 'en_content' =>   $request->input('en_content'),
                 
             );

   
   // Validator::make($request->all(), $validate)->validate();
    $image = FileUpload::uploadFile($request, 'image', 'uploads/image');
      if($image != ""){
          $data['image'] = $image; 
      }


   Content::where('slug', 'who-we-are')->update($data);

   Session::flash('msg', 'Data has been updated!' );
   return redirect()->back(); 
 }



 public function view(){ 

   $data = Content::select('*')
   ->where('slug', 'who-we-are')
   ->first();

   if(!empty($data)){
     return view('cp.history.edit', ['route'=>$this->route,'data'=>$data]);
   }else{
     return response(view('errors.404'), 404);
   }
 }

}
  
