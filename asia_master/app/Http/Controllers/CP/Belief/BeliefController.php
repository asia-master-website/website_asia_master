<?php

namespace App\Http\Controllers\CP\Belief;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Contents as Content;


class BeliefController extends Controller
{
    
  function __construct (){
    $this->route = "cp.belief";
 }

 public function update(Request $request){   
   $id = $request->input('id');
   $image = "";
  
   $data = array( 
                
            'kh_content_part1' =>   $request->input('kh_content_part1'),
            'en_content_part1' =>   $request->input('en_content_part1'),
            'kh_content_part2' =>   $request->input('kh_content_part2'),
            'en_content_part2' =>   $request->input('en_content_part2'),
                 
             );

   
    //Validator::make($request->all(), $validate)->validate();
   $image = FileUpload::uploadFile($request, 'image', 'uploads/image');
      if($image != ""){
          $data['image'] = $image; 
      }


   Content::where('slug', 'belief')->update($data);

   Session::flash('msg', 'Data has been updated!' );
   return redirect()->back(); 
 }



 public function view(){ 

   $data = Content::select('*')
   ->where('slug', 'belief')
   ->first();

   if(!empty($data)){
     return view('cp.belief.edit', ['route'=>$this->route,'data'=>$data]);
   }else{
     return response(view('errors.404'), 404);
   }
 }

}
  
