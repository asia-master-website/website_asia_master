<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Contents;
use App\Model\Banner;

class AboutController extends FrontendController
{
    public function index($locale = "en")
    {
        $defaultData = $this->defaultData($locale);
        $whatWeSay = Contents::select($locale . '_title as title', $locale . '_content as content', 'image')
            ->where('slug', 'what-we-say')
            ->first();
        $whoWeare = Contents::select($locale . '_title as title', $locale . '_content as content', 'image')
            ->where('slug', 'who-we-are')
            ->first();
        $missionAndVission = Contents::select($locale.'_title as title', $locale.'_content as content', 'image')
            ->where('slug', 'mission-and-vision')
            ->first();
        $banner = Banner::select('image', 'is_published')->find(1);

        return view('frontend.about', ['locale' => $locale, 'whatWeSay' => $whatWeSay, 'whoWeare' => $whoWeare, 'banner' => $banner, 'missionAndVission' => $missionAndVission, 'defaultData' => $defaultData]);
    }
}
