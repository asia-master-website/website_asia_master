<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Slide;
use App\Model\Customers;
use App\Model\Partners;
use App\Model\Services;
use App\Model\Banner;
use App\Model\Contents;

class HomeController extends FrontendController
{
    
public function index($locale = "en") {
    $defaultData = $this->defaultData($locale);
    $banner = Banner::select('image','is_published')->find(3);
    $slide = Slide::select('id',$locale.'_title as title',$locale.'_description as description','image')->where('is_published', 1)->get();
    $partners = Partners::select('id','image','url')->where('is_published',1)->get();
    $customers = Customers::select($locale.'_title as title',$locale.'_description as description','image','id')->where('is_published',1)->get();
    $services = Services::select($locale.'_title as title','image',$locale.'_content as content')->where('is_published',1)->limit(4)->get();

    $whatWeSay = Contents::select($locale.'_title as title', $locale.'_content as content', 'image')
		->where('slug', 'what-we-say')
        ->first();
    $whoWeare = Contents::select($locale.'_title as title', $locale.'_content as content', 'image')
    ->where('slug', 'who-we-are')
    ->first();
    $missionAndVission = Contents::select($locale.'_title as title', $locale.'_content as content', 'image')
    ->where('slug', 'mission-and-vision')
    ->first();
    $belief = Contents::select ( $locale.'_title as title',$locale.'_content_part1 as content_part1', $locale.'_content_part2 as content_part2', 'image' )
    ->where('slug','belief')
    ->first();
        
    return view('frontend.home',
    ['locale'     => $locale, 
    'slide'       => $slide,
    'partners'    => $partners,
    'customers'   =>$customers,
    'belief'      =>$belief,
    'services'    =>$services,
    'banner'      =>$banner,
    'defaultData' => $defaultData,
    'whatWeSay'   => $whatWeSay,
    'whoWeare'    => $whoWeare,
    'missionAndVission' => $missionAndVission,
    ]);
 }

}