<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
  
use App\Model\Contents;

class HistorydetailController extends FrontendController
{
    
    public function index($locale = "en") {
            $defaultData = $this->defaultData($locale);
            $whoWeare = Contents::select($locale.'_title as title', $locale.'_content as content', 'image')
            ->where('slug', 'who-we-are')
            ->first();
        return view('frontend.historydetail',['locale'=> $locale,
        'whoWeare'    => $whoWeare,
        'defaultData' => $defaultData
        ]);
  
 }

}