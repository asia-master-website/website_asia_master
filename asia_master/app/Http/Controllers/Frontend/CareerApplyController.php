<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Validator;
use Session;
use App\Model\CareerApply;
use App\Model\Care;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
class CareerApplyController extends FrontendController
{
    
    public function read($locale = "en", $id=0 ) {

        $defaultData = $this->defaultData($locale);
        $data = Care::select('id','image', $locale.'_title as title',$locale.'_description as description','is_published')
        ->where(['id'=>$id])->first();
    return view(' frontend.career.career-detail',
    [
        'locale'      =>$locale,
        'defaultData' =>$defaultData,
        'data'        =>$data,
    ]);
 }

    public function store(Request $request, $locale){
      
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'career_id'                =>       $request->input('career_id'),
            'name'                      =>      $request->input('name'),
            'email'                     =>      $request->input('email'),
            'phone'                     =>      $request->input('phone'),
            'created_at'                =>      $now
        );
       

        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [ 
                'career_id'  => 'required',
                'name'       => 'required|max:60',
                'phone'      => 'required|max:13',
                'email'      => 'required|max:50',
                'cv'      => [
                                'mimes:pdf',
                                'max:1000'
                            ],
                'g-recaptcha-response' => 'required',
            ], 

            [
                
            ])->validate();

        $cv = FileUpload::uploadFile($request, 'cv', 'uploads/career-apply');
        if($cv != ""){
            $data['cv'] = $cv; 
        }

        $id = CareerApply::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        if($request->page == 'frontend.career-detail'){
            return redirect()->back();
        }else{
            return redirect($locale.'/career/'.$request->input('career_id').'#sent-career-detail');
        }
        
    }
}