<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Care;
use App\Model\Banner;

class CareerController extends FrontendController
{
    
    public function index($locale = "en") {
        $care = Care::select('id','image',$locale.'_title as title',$locale.'_description as description',$locale.'_location as location','deadline','position','type')->where('is_published', 1)->get();
        $banner = Banner::select('image','is_published')->find(1);
        $defaultData = $this->defaultData($locale);

        return view('frontend.career.career',
        ['locale'     => $locale, 
        'care'        =>$care,
        'banner'      =>$banner,
        'defaultData' => $defaultData
        ]);
  
 }

 public function read($locale = "en", $id=0 ) {

        $defaultData = $this->defaultData($locale);
        $data = Care::select('id','image', $locale.'_title as title',$locale.'_description as description','is_published')
        ->where(['id'=>$id])->first();
    return view(' frontend.career.career-detail',
    [
        'locale'      =>$locale,
        'defaultData' =>$defaultData,
        'data'        =>$data,
    ]);
 }
}