<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
  
  
class HistoryController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        return view('frontend.history',['locale'=> $locale, 'defaultData' => $defaultData]);
  
 }
}