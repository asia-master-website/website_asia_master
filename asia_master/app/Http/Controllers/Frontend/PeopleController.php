<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Peoples as Peoples;
use App\Model\Manager as Manager;
use App\Model\Staff as Staff;
use App\Model\Banner;

class PeopleController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('image','is_published')->find(5);
        $manager = Manager::select('id',$locale.'_title as title',$locale.'_description as description','role','image')->where('is_published', 1)->get();
        $peoples = Peoples::select('id',$locale.'_title as title',$locale.'_description as description','role','image')->where('is_published', 1)->get();
        $staff = Staff::select('id',$locale.'_title as title',$locale.'_description as description','image')->where('is_published', 1)->get();
        

        return view('frontend.people',['locale'=> $locale,
        'manager'     =>$manager,
        'peoples'     =>$peoples,
        'banner'      =>$banner,
        'staff'       =>$staff,
        'defaultData' => $defaultData
        ]);
  
 }
}