<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

use App\Model\Setup\Commune as Commune;

class FrontendController extends Controller
{
    
    public $defaultData = array();
    public function __construct(){
      
    }

    public function defaultData($locale = "en"){


    	App::setLocale($locale);

        //Current Language
        $parameters = Route::getCurrentRoute()->parameters();

        $enRouteParamenters = $parameters;
        $enRouteParamenters['locale'] = 'en';
        $this->defaultData['enRouteParamenters'] = $enRouteParamenters;
        $khRouteParamenters = $parameters;
        $khRouteParamenters['locale'] = 'kh';
        $this->defaultData['khRouteParamenters'] = $khRouteParamenters;
        $this->defaultData['routeName'] = Route::currentRouteName();

        return $this->defaultData;
    }

 }