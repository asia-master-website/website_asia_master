<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Validator;
use Session;
use App\Model\Message;
use App\Model\Slidecontact as Slideaboutus;

class ContactController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        return view('frontend.contact',
        ['locale'=> $locale,'defaultData' => $defaultData]);
    }

    public function store(Request $request, $locale){
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
           
            'name'                      =>      $request->input('name'),
            'email'                     =>      $request->input('email'),
            'phone'                     =>      $request->input('phone'),
            'message'                   =>      $request->input('message'),
            'created_at'                =>      $now
        );
       

        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ], 

            [
                
            ])->validate();

        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        if($request->page == 'frontend.contact'){
            return redirect()->back();
        }else{
            return redirect($locale.'/contact#sent-contact');
        }
        
    }
}