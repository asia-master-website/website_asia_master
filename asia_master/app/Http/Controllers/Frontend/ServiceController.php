<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
  
use App\Model\Services;
use App\Model\Banner;

class ServiceController extends FrontendController
{
    
    public function index($locale = "en") {
       
       $services = Services::select($locale.'_title as title','image',$locale.'_content as content','is_position')->where('is_published',1)->get();
       $banner = Banner::select('image','is_published')->find(6);
       $defaultData = $this->defaultData($locale);
        return view('frontend.service',
            ['locale'     => $locale,
            'services'    =>$services,
            'banner'      =>$banner,
            'defaultData' => $defaultData
            ]);
  
 }
}