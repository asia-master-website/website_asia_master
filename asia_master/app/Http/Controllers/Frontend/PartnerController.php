<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Banner;
use App\Model\Partners as Partners;

class PartnerController extends FrontendController
{
    
    public function index($locale = "en") {
        $partners = Partners::select('id','image','url')->where('is_published',1)->get();
        $banner = Banner::select('image','is_published')->find(4);
        $defaultData = $this->defaultData($locale);
        return view('frontend.partner',
        ['locale'     => $locale,
        'partners'    => $partners,
        'banner'      =>$banner,
        'defaultData' => $defaultData
        ]);
  
 }
}