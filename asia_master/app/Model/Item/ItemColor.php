<?php

namespace App\Model\Item;
use Illuminate\Database\Eloquent\Model;

class ItemColor extends Model
{
   
    protected $table = 'item_colors';
    public function sizes(){
        return $this->hasMany('App\Model\Item\ItemColorSize', 'color_id');
    }
}
