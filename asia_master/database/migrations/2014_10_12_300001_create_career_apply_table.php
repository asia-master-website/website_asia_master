<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerApplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_apply', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->nullable();
            $table->string('name', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->text('email')->nullable();
            $table->text('description')->nullable();
            $table->string('cv', 250)->nullable();
            $table->string('vertical_image', 250)->nullable();
            $table->boolean('is_published')->default(0);
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_apply');
    }
}
