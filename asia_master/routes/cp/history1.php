<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'HistoryController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'HistoryController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'HistoryController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'HistoryController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'HistoryController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'HistoryController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'HistoryController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'HistoryController@updateStatus']);
});	