<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});
		
		Route::group(['as' => 'slide.',  'prefix' => 'slide', 'namespace' => 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
		Route::group(['as' => 'banner.',  'prefix' => 'banner', 'namespace' => 'Banner'], function () {
			require(__DIR__.'/banner.php');
		});
		Route::group(['as' => 'vision.',  'prefix' => 'vision', 'namespace' => 'Vision'], function () {
			require(__DIR__.'/vision.php');
		});

		Route::group(['as' => 'mission.',  'prefix' => 'mission', 'namespace' => 'Mission'], function () {
			require(__DIR__.'/mission.php');
		});

		Route::group(['as' => 'customers.',  'prefix' => 'customers', 'namespace' => 'Customers'], function () {
			require(__DIR__.'/customers.php');
		});

		Route::group(['as' => 'belief.',  'prefix' => 'belief', 'namespace' => 'Belief'], function () {
			require(__DIR__.'/belief.php');
		});

		Route::group(['as' => 'partners.',  'prefix' => 'partners', 'namespace' => 'Partners'], function () {
			require(__DIR__.'/partners.php');
		});

		Route::group(['as' => 'peoples.',  'prefix' => 'peoples', 'namespace' => 'Peoples'], function () {
			require(__DIR__.'/peoples.php');
		});

		Route::group(['as' => 'staff.',  'prefix' => 'staff', 'namespace' => 'Staff'], function () {
			require(__DIR__.'/staff.php');
		});

		Route::group(['as' => 'manager.',  'prefix' => 'manager', 'namespace' => 'Manager'], function () {
			require(__DIR__.'/manager.php');
		});



		Route::group(['as' => 'services.',  'prefix' => 'services', 'namespace' => 'Services'], function () {
			require(__DIR__.'/services.php');
		});

	    
		Route::group(['as' => 'history.',  'prefix' => 'history', 'namespace' => 'History'], function () {
			require(__DIR__.'/history.php');
		});

		 
		Route::group(['as' => 'care.',  'prefix' => 'care', 'namespace' => 'Care'], function () {
			require(__DIR__.'/care.php');
		});

		Route::group(['as' => 'message.',  'prefix' => 'message', 'namespace' => 'Message'], function () {
			require(__DIR__.'/message.php');
		});
													// Content part route							
		Route::group(['as' => 'chairman.',  'prefix' => 'chairman', 'namespace' => 'Chairman'], function () {
			require(__DIR__.'/chairman.php');
		});
		Route::group(['as' => 'mission-and-vission.',  'prefix' => 'mission-and-vission.', 'namespace' => 'MissionandVission'], function () {
			require(__DIR__.'/missionandvission.php');
		});

		Route::group(['as' => 'careerapply.',  'prefix' => 'careerapply', 'namespace' => 'careerapply'], function () {
			require(__DIR__.'/careerapply.php');
		});
	});