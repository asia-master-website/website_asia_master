<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'MissionandVissionController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'MissionandVissionController@update']);
