<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Message

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'MessageController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'MessageController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'MessageController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'MessageController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'MessageController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'MessageController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'MessageController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'MessageController@updateStatus']);
});	