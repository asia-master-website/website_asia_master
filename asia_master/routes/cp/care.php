<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Careere 

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CareController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'CareController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CareController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CareController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CareController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CareController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CareController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CareController@updateStatus']);
});	