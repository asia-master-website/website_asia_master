<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'BeliefController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'BeliefController@update']);
