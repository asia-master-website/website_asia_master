<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ChairmanController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ChairmanController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ChairmanController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ChairmanController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ChairmanController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ChairmanController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ChairmanController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ChairmanController@updateStatus']);
});	