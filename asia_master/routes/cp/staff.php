<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> People

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'StaffController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'StaffController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'StaffController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'StaffController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'StaffController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'StaffController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'StaffController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'StaffController@updateStatus']);
});	