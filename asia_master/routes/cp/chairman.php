<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'ChairmanController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'ChairmanController@update']);
