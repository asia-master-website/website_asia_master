<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Partner

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'PartnersController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'PartnersController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PartnersController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'PartnersController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'PartnersController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PartnersController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'PartnersController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'PartnersController@updateStatus']);
});	