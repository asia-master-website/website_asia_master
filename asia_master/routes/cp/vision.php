<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'VisionController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'VisionController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'VisionController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'VisionController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'VisionController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'VisionController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'VisionController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'VisionController@updateStatus']);
});	