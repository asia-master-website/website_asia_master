<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Message

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CareerApplyController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'CareerApplyController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CareerApplyController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CareerApplyController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CareerApplyController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CareerApplyController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CareerApplyController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CareerApplyController@updateStatus']);
});	