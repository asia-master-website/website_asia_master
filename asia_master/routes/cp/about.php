<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'AboutController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'AboutController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'AboutController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'AboutController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'AboutController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'AboutController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'AboutController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'AboutController@updateStatus']);
});	