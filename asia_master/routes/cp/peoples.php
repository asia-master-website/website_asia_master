<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> People

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'PeoplesController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'PeoplesController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PeoplesController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'PeoplesController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'PeoplesController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PeoplesController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'PeoplesController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'PeoplesController@updateStatus']);
});	