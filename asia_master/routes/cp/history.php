<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'HistoryController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'HistoryController@update']);
