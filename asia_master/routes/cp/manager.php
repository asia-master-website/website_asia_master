<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manager

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ManagerController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ManagerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ManagerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ManagerController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ManagerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ManagerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ManagerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ManagerController@updateStatus']);
});	