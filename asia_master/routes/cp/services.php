<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ServicesController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'ServicesController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ServicesController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ServicesController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ServicesController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ServicesController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ServicesController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ServicesController@updateStatus']);
});	