<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'SlideController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'SlideController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'SlideController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'SlideController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'SlideController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'SlideController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'SlideController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'SlideController@updateStatus']);
});	