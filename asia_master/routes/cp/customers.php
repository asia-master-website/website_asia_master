<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mission

Route::group([], function () {
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CustomersController@create']);
	Route::get('/', 				['as' => 'index', 			'uses' => 'CustomersController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CustomersController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CustomersController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CustomersController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CustomersController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CustomersController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CustomersController@updateStatus']);
});	