<?php
Route::get('{locale}/home',            [ 'as' => 'home',               'uses' => 'HomeController@index']);
Route::get('{locale}/about',           [ 'as' => 'about',              'uses' => 'AboutController@index']);
Route::get('{locale}/service',         [ 'as' => 'service',            'uses' => 'ServiceController@index']);
Route::get('{locale}/partner',         [ 'as' => 'partner',            'uses' => 'PartnerController@index']);
Route::get('{locale}/career',          [ 'as' => 'career',             'uses' => 'CareerController@index']);
Route::get('{locale}/career/{id}',     [ 'as' => 'career-detail',      'uses' => 'CareerController@read']);
Route::get('{locale}/people',          [ 'as' => 'people',             'uses' => 'PeopleController@index']);
Route::get('{locale}/contact',         [ 'as' => 'contact',            'uses' => 'ContactController@index']);
Route::get('{locale}/chairmandetail',  [ 'as' => 'chairmandetail',     'uses' => 'ChairmandetailController@index']);
Route::get('{locale}/historydetail',   [ 'as' => 'historydetail',      'uses' => 'HistorydetailController@index']);
Route::put('{locale}/submit-contact',  [ 'as' => 'submit-contact',     'uses' => 'ContactController@store']);
Route::put('{locale}/submit-careerapply',  [ 'as' => 'submit-careerapply',     'uses' => 'CareerApplyController@store']);
