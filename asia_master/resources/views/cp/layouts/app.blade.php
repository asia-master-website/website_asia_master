@extends('cp.layouts.master')

@section ('headercss')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/images/favicon/sdf-logo.png') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/summernote/summernote.css') }}"/>
    <link rel="stylesheet" href="{{ asset ('public/cp/css/main.css') }}">
    <script type="text/javascript" src="{{ asset ('public/cp/js/lib/jquery/jquery.min.js') }}"></script>
    @yield('appheadercss')
@endsection



@section ('bodyclass')
    class="with-side-menu control-panel control-panel-compact"
@endsection

@section ('header')

<header class="site-header">
    <div class="container-fluid">
        <a target="_blank" href="{{ url('/') }}" class="site-logo">
            <img class="hidden-md-down" src="{{ asset ('public/cp/img/logo.png') }}" alt="">
            <img class="hidden-lg-up" src="{{ asset ('public/cp/img/logo.png') }}" alt="">
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset (Auth::user()->avatar) }}" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="{{ route('cp.user.profile.edit') }}"><span class="fa fa-user"></span> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('cp.auth.logout') }}"><span class="fa fa-sign-out"></span> Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
@endsection

@section ('menu')
    @php ($menu = "")
    @if(isset($_GET['menu']))
        @php( $menu = $_GET['menu'])
    @endif
    

    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">   
            <li class="@yield('active-main-menu-home') red with-sub">
                <span>
                    <i class=" font-icon fa fa-home"></i>
                    <span class="lbl">Home</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.slide.index') }}"><span class="lbl">Slide</span></a></li> 
                    <li class=""><a href="{{ route('cp.banner.index') }}"><span class="lbl">Banner</span></a></li> 
                    <li class=""><a href="{{ route('cp.belief.view', 'belief') }}"><span class="lbl">Our Belief</span></a></li>
                    <li class=""><a href="{{ route('cp.customers.index') }}"><span class="lbl">Our Customer message</span></a></li>


                </ul>
            </li>

            <li class="@yield('active-main-menu-about') red with-sub">
                <span>
                    <i class="  font-icon fa fa-list-alt"></i>
                    <span class="lbl">About us</span>
                </span>
                <ul>
                    <li class=""><a href="{{route('cp.chairman.view', 'what-we-say')}}"><span class="lbl">Chair Man</span></a></li>
                    <li class=""><a href="{{route('cp.history.view', 'who-we-are')}}"><span class="lbl">History</span></a></li>
                    <li class=""><a href="{{route('cp.mission-and-vission.view', 'mission-and-vision')}}"><span class="lbl">Mission and Vission</span></a></li>                                                                             
                </ul>
            </li>

            <li class="@yield('active-main-menu-peoples') red with-sub">
                <span>
                    <i class=" font-icon font-icon-user"></i>
                    <span class="lbl">Our People</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.manager.index') }}"><span class="lbl">Manager</span></a></li>                                              
                    <li class=""><a href="{{ route('cp.peoples.index') }}"><span class="lbl">Peoples</span></a></li>                                              
                    <li class=""><a href="{{ route('cp.staff.index') }}"><span class="lbl">Our Staff</span></a></li>                                              
                </ul>
            </li>

            <li class="@yield('active-main-menu-services') red with-sub">
                <span>
                    <i class=" font-icon fa fa-archive"></i>
                    <span class="lbl">Our Services</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.services.index') }}"><span class="lbl">Services</span></a></li>                                                      
                </ul>
            </li>

            <li class="@yield('active-main-menu-partnes') red with-sub">
                <span>
                    <i class=" font-icon font-icon-user"></i>
                    <span class="lbl">Our Partners</span>
                </span>
                <ul>
                <li class=""><a href="{{ route('cp.partners.index') }}"><span class="lbl">Partner</span></a></li>                                                                      
                </ul>
            </li>

            <li class="@yield('active-main-menu-care') red with-sub">
                <span>
                    <i class=" font-icon font-icon-case-2"></i>
                    <span class="lbl">Career Opportunity</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.care.index') }}"><span class="lbl">Career Opportunity</span></a></li>  
                    <li class=""><a href="{{ route('cp.careerapply.index') }}"><span class="lbl">Career Apply</span></a></li>                                                 
                </ul>
            </li>
            <li class="@yield('active-main-menu-message') red with-sub">
                <a href="{{ route('cp.message.index') }}">
                <span>
                    <i class="fa fa-envelope"></i>
                    <span class="lbl">message</span>
                </span>
            </a>
            </li>
            @if(Auth::user()->position_id == 1)
             <li class="red @yield('active-main-menu-user')">
                <a href="{{ route('cp.user.user.index') }}">
                <span>
                    <i class="fa fa-users"></i>
                    <span class="lbl">Users</span>
                </span>
                </a>
            </li>
            @endif

           
        </ul>
    </nav><!--.side-menu-->

@endsection

@section ('content')
    <div class="page-content">
        
        @yield ('page-content')
        
    </div>
@endsection




@section ('bottomjs')
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        @yield ('imageuploadjs')
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/tether/tether.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/plugins.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/lobipanel/lobipanel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/blockUI/jquery.blockUI.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset ('public/cp/js/lib/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{ asset ('public/cp/js/lib/select2/select2.full.min.js')}}"></script>
       <script type="text/javascript" src="{{ asset ('public/cp/js/lib/summernote/summernote.min.js') }}"></script>
        <script src="{{ asset ('public/cp/js/app.js') }}"></script>
        <script src="{{ asset ('public/cp/js/camcyber.js') }}"></script>
        @yield('appbottomjs')

        @if(Session::has('msg'))
        <script type="text/JavaScript">
            toastr.success("{!!Session::get('msg')!!}");
        </script>
        @endif
        @if(Session::has('error'))
        <script type="text/JavaScript">
            toastr.error("{!!Session::get('error')!!}");
        </script>
        @endif
@endsection