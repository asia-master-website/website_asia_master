<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset ('public/frontend/assets/images/favicon.png')}}" rel="shortcut icon">
   
    <!-- ========================================= Css files -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/owl.theme.default.min.html') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/camcyber.css') }}">
    <!-- <link href="{{ asset ('public/frontend/css/owl.theme.css') }}" rel="stylesheet">  -->
    <title>Asia Master (Cambodia) Co., LTD</title>
</head>

<body>
    <!-- =========================== header-->
    <header class="header has-style2 section">
       
        <!-- =========================== navbar-->
        <nav class="navbar is-dark">
            <div class="container">
                <div class="flex">
                    <a href="{{route('home',['locale'=>$locale])}}" class="navbar-brand flex vcenter">
                        <img data-aos="fade-right" class="logo" src="{{ asset('public/frontend/assets/img/logo1.png') }}" style="width: 165px; height: auto;">
                    </a>
                    <ul class="navbar-menu">
                        
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-about')"
                            href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-service')"
                            href="{{route('service',['locale'=>$locale])}}">{{__('general.our-services')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-people')"
                            href="{{route('people',['locale'=>$locale])}}">{{__('general.our-people')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-partner')"
                            href="{{route('partner',['locale'=>$locale])}}">{{__('general.our-partners')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-career')"
                            href="{{route('career',['locale'=>$locale])}}">{{__('general.career-opportunity')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-contact')"
                        href="{{route('contact',['locale'=>$locale])}}">{{__('general.Contact-us')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="level-right">
                    <!-- your list menu here -->
                    <div class="navbar-menu">
                       
                        <a> 
                        <a href="{{route($defaultData['routeName'], $defaultData['khRouteParamenters'])}}"> <img src="{{ asset('public/frontend/assets/img/logo/bng.png') }}" alt=""></a>
                        <a href="{{route($defaultData['routeName'], $defaultData['enRouteParamenters'])}}"> <img src="{{ asset('public/frontend/assets/img/logo/hgf.png') }}" alt=""></a>
                            
                        </a>
                    </div>
                    <div class="mobile-menu">
                        <!-- your list menu in mobile here -->
                        <ul>
                            <li><a class="fade-page" href="home_1.html">Home 1</a>
                            </li>
                            <li><a class="fade-page" href="home_2.html">Home 2</a>
                            </li>
                            <li><a class="fade-page" href="home_3.html">Home 3</a>
                            </li>
                            <li><a class="fade-page" href="home_4.html">Home 4</a>
                            </li>
                            <li><a class="fade-page" href="home_5.html">Home 5</a>
                            </li>
                            <li><a class="fade-page" href="home_1_grad.html">Home 1 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_2_grad.html">Home 2 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_3_grad.html">Home 3 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_4_grad.html">Home 4 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_5_grad.html">Home 5 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="flex">
                        <div class="menu-toggle-icon">
                            <div class="menu-toggle">
                                <div class="menu">
                                    <input type="checkbox">
                                    <div class="line-menu"></div>
                                    <div class="line-menu"></div>
                                    <div class="line-menu"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
     
    </header>

    @yield('content')
            

 <!--==============================================================> footer -->
<!-- <footer class="footer has-style1">
    <div class="footer-head">
        <img class="rounded sm-hidden " src="https://digikit.netlify.app/assets/images/others/hero-round.svg" alt="">
        <div class="overflow-hidden">
        </div>
        <button id="button" class="to-top">
        <img class="footer-icon-style" src="{{ asset('public/frontend/assets/img/logofooter.png') }}" alt="">
        </button>
        <div class="section is-sm ">
            <div class="container">
                <h2 class="section-title is-center text-white">{{__('general.contact-us-now') }}</h2>
                <div class="flex center mt-5">
                    <form class=" bg-transparent ">
                        <input type="text" class="footer-input" placeholder="{{__('general.enter-your-email-to-contact us') }}">
                        <li><a href="{{route('contact',['locale'=>$locale])}}"class="btn btn-primary btn-round footer-btn">{{__('general.Send') }} </button></a></li>
                    </form>
                </div>
                <div class="row footer-contact min-30">
                    <div class="col-lg-4 col-md-4">
                        <div class="contact-item">
                            <h6>{{__('general.phone') }}</h6>
                            <a href="tel:+855 23 667 6666"><p class="contact-item-info">+855 23 667 6666</p></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="contact-item">
                            <h6>{{__('general.email') }}</h6>
                            <p class="contact-item-info">info@asiamaster.net</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="contact-item">
                            <h6>{{__('general.Addresss') }}</h6>
                            <p class="contact-item-info">{{__('general.Address') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer> -->
 
 <footer class="footer has-style2">
    <div class="footer-body">
        <div class="container">
            <div class="row ">
                <div class="col-lg-4">
                    <div class="footer-desc">
                        <div class="logo">
                            <a href="{{route('home',['locale'=>$locale])}}"><img src="{{ asset('public/frontend/assets/img/logofooter.png') }}" alt=""></a>
                        </div>
                        <p>{{__('general.Address') }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <h6 class="list-title">{{__('general.our-services')}}</h6>
                    <ul class="list-items">
                        <li> <a href="{{route('service',['locale'=>$locale])}}">*{{__('general.24-7day')}}</a> </li>
                        <li> <a href="{{route('service',['locale'=>$locale])}}">*{{__('general.multilingual-outbound-call-center')}}</a> </li>
                        <li> <a href="{{route('service',['locale'=>$locale])}}">*{{__('general.tele-customer-care-system')}}</a> </li>
                        <li> <a href="{{route('service',['locale'=>$locale])}}">*{{__('general.customer-service-solution')}}</a> </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-sm-4 col-6">
                    <h6 class="list-title">{{__('general.Page') }}</h6>
                    <ul class="list-items">
                        <li> <a 
                            href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a> 
                         </li>
                         <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page"
                            href="{{route('service',['locale'=>$locale])}}">{{__('general.our-services')}}</a>
                        </li>
                         <li> <a 
                            href="{{route('people',['locale'=>$locale])}}">{{__('general.our-people')}}</a>
                         </li>
                        
                        <li> <a  href="{{route('partner',['locale'=>$locale])}}">{{__('general.our-partners')}}</a> </li>
                        <li> <a   href="{{route('career',['locale'=>$locale])}}">{{__('general.career-opportunity')}}</a> </li>
                         </li>
                        <li> <a href="{{route('contact',['locale'=>$locale])}}">{{__('general.Contact-us')}}</a> </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-sm-4 col-6">
                    <h6 class="list-title">{{__('general.working-hours') }}</h6>
                    <ul class="list-items">
                        <li> <a>24 Hour / 7 Days</a> </li>
                        <li> <a>Monday - Sunday</a> </li>
                        <li> <a>+855 23 667 6666</a> </li>
                        <li> <a>info@asiamaster.net</a> </li>
                    </ul>
                </div>
                
                <div class="col-lg-12 center">
                    <!-- <h2 class="section-title is-center text-center text-white">{{__('general.contact-us-now') }}</h2> -->
                    <div class="flex center mt-5">
                        <form class=" bg-transparent ">
                            <input type="text" class="footer-input" placeholder="{{__('general.enter-your-email-to-contact us') }}">
                            <li><a href="{{route('contact',['locale'=>$locale])}}"class="btn btn-primary btn-round footer-btn">{{__('general.Send') }} </button></a></li>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p class="copyright text-center text-copyright">© 2020 Asia Master Cambodia Co.,Ltd. All Right Reserved</p>
    </div>
</footer>

<!-- ====================================== js files  -->
<script src="{{ asset('public/frontend/assets/js/plugins/jQuery.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/modernizr.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/feather-icons.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/slick.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/owl.carousel.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/aos.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/typed.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/all.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/jquery.counterup.min.js') }}"></script>
<!-- <script src="../../unpkg.com/ionicons%404.5.10-0/dist/ionicons.js') }}"></script> -->
<script src="{{ asset('public/frontend/assets/js/main.js') }}"></script>
</body>
</html>