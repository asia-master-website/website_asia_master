@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-career', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')

<div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/career.png') }}" alt="" style="width:100%" >
            <br />
        </div>

        <div class="container page-container"  >

            <div class="row job-table-header">
                        
                    
                <div class="col-6 text-center " >
                    
                    <span class=""><b>Job Title</b></span>
                    
                </div>

                <div class="col-2 " >
                    <span class=""><b>Location</b></span>
                  
                </div>

                <div class="col-2 " >
                    <span class=""><b>Type</b></span>
                    
               
                </div>

                <div class="col-2 " >
              
                 
                </div>

                
            </div>

            @foreach( $care as $row)

                <div class="row">
                    
                    <div class="col-2 text-center">
                        <br />
                        <img class="about-img img-fluid" style="max-width:80px" src="{!! asset ($row->image) ?? '' !!}" alt="">
                    </div>

                    <div class="col-4 " >
                        <br />
                        <h6 class="">{!! $row->title !!}</h6>
                        <p class="" >
                            - Deadline: <b>{!! $row->deadline !!}</b> <br />
                            - Number of Positions: <b>{!! $row->position !!}</b>
                        </p>
                    </div>

                    <div class="col-2 " >
                        <br />
                        <p class="" >
                            {!! $row->location !!}
                        </p>
                    </div>

                    <div class="col-2 " >
                        <br />
                      
                        <p class="" >
                            {!! $row->type !!}
                        </p>
                    </div>

                    <div class="col-2 " >
                        <br />
                      
                        <a href="{{route('career-detail',['locale' => $locale, 'id'=>$row->id])}}"> <p class="" >
                            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/apply.png') }}" alt="" style="width:100%" >
                        </p></a>
                    </div>

                  

                    <div class="col-12">
                        <hr />
                    </div>
                    
                </div>

            @endforeach

        </div>
    </div>
 
@endsection