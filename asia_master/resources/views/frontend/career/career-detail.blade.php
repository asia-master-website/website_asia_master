@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-career', 'active')


@section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  <script type="text/javascript">
    $(document).ready(function(){
      $("#submit-contact").submit(function(event){
        submit(event);
      })

      @if (Session::has('invalidData'))
        $("#form").show();
      @endif

    })
	@if(Session::has('msg'))
			toastr.success("{{ Session::get('msg') }}");
	@endif



    function submit(event){
      
	  name = $("#name").val();
      email = $("#email").val();
      phonenumber = $("#phonenumber").val();
      message = $("#message").val();
      recaptcha =$('#g-recaptcha-response').val();

      if(name != ''){
			
      if(validateEmail(email)){

        if(phone != ''){

        }else{
          toastr.error("Please enter your phone");
          event.preventDefault();
          $("#email").focus();
        }

      }else{
          toastr.error("Please give us correct email");
          event.preventDefault();
          $("#email").focus();
      }
              
      }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#fristname").focus();
      }
    }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#lastname").focus();
      }
    }
    
    function showApplicationForm(){
      form = $("#form");
      if(form.is(":visible")){
        form.hide();
      }else{
        form.show();
      }
    }

    
      function validatePhone(phone) {
          return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
      }

</script>
@endsection
@section ('about')
@endsection

@section ('content')
  
    <div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/about-us.jpg') }}" alt="" style="width:100%" >
            <br />
        </div>
        <div class="container page-container"  >

            <section class="section section-about">
                <div class="container-fluid">
                   
                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Career Opportunity
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 ceo-message">
                            <div>
                                <h2 style="color:#1968b2;"><b>{!!$data->description ?? '' !!}</b></h2>
                                <h5><i>Date: 03/04/2020</i></h5>
                            </div>
                        </div>
                    
                    </div>
                    
                </div>
                <div class=" col-12  ">
                    <div style="border: 1px solid #d3d3d3;"class="container-fluid">
                        <div class="row">
                            <div class="col " >
                                <h4> Submit Your CV</h4>
                            </div>
                        </div>
                        @if(Session::has('msg'))
                            <div class="pd-buttom">
                                <div class="alert alert-success" role="alert">
                                Your request has been sent! We will respone you soon.
                                </div>
                            </div>
                        @endif
    
                        <form class="contact-form" id="submit-care" action="{{ route('submit-careerapply', $locale) }} " method="post" enctype="multipart/form-data">
                            {{ csrf_field() }} 
                            {{ method_field('PUT') }}
                            <input hidden name="career_id"  placeholder="" value="{{$data->id}}" class="form-control has-style1" id="career_id"
                                        type="text">
                                <div class="form-group">
                                    <label>{{__('general.your-name') }}</label>
                                    <input name="name"  placeholder="Name " class="form-control has-style1" id="name"
                                        type="text">
                                    @if ($errors->has('name'))
                                        <div class="pd-buttom">
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('name') }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{__('general.your-phone-number') }}</label>
                                    <input name="phone" placeholder="Phone Number " class="form-control has-style1" id="phone"
                                        type="number">

                                    @if ($errors->has('phone'))
                                        <div class="pd-buttom">
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('phone') }}
                                            </div>
                                        </div>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label>{{__('general.Email') }}</label>
                                    <input name="email" placeholder=" example@mail.com" class="form-control has-style1" id="email"
                                        type="text">

                                    @if ($errors->has('email'))
                                        <div class="pd-buttom">
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('email') }}
                                            </div>
                                        </div>
                                    @endif

                                </div>
                             
                                <div>
                                <div class="col-xs-6 col-sm-6 col-md-6" >
                                    <div class="form-group">
                                       <input  id="file"  name="cv" type="file"  accept="application/pdf">
                                        <br />
                                        Only PDF file is accepted and maximum 1M.
                                    </div>
                                    @if ($errors->has('cv'))
                                        <div class="pd-buttom">
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('cv') }}
                                            </div>
                                        </div>
                                    @endif
                                       
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6" >
                                        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                        <div class="g-recaptcha" data-sitekey="6LdBDbMUAAAAAHKzSXVrtACFD5qBgeXyGHkvsTGy"></div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <div class="pd-buttom">
                                                <div class="alert alert-danger" role="alert">
                                                    {{ $errors->first('g-recaptcha-response') }}
                                                </div>
                                            </div>
                                        @endif   
                                </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 text-right" >
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-round" value="Submit">
                                        <span style="padding-left: 10px;"> {{__('general.send-me') }} → </span>
                                    </button>
                                </div>
                            </div>
                            </form>
                    </div>
                </div>
            </section>

            
        </div>
    </div>
@endsection