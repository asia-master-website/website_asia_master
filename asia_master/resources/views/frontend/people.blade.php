@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-people', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

    <div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/team.png') }}" alt="" style="width:100%" >
            <br />
        </div>
        <div class="container page-container"  >

        <section class="section section-about">
                <div class="container-fluid">
                   
                    <div class="row">
                        <div class="col-12 page-section-title ">
                            CEO's Message
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 ceo-message">
                            <div>
                                <h2 class="">Welcome to Asia Master Cambodia!</h2>
                                <p class="" style="font-size: 18px">
                                <br />
                                &nbsp; &nbsp; <q>Asia Master Dear Fellow Colleagues, It has been more than ten years since the establishment of this company. Asia Master CAMBODIA was founded with long-range vision and dear-stated mission. As a founder, I am truly proud and appreciated to all management levels and fellow colleagues for the priceless, ongoing, physical and mental effort which has guaranteed this company's success and sustainability to date. Such outcome is definitely not a coincidence but a result of integrated factors such as our hard work, patience, honestly, loyalty, mutual understanding, unity so on and so forth. </q>
                                </p>
                            </div>

                            <div class="text-right" >
                                <p>
                                    <br />
                                    <b style="font-size: 20px" >Chheang Sopheak</b>
                                    <br />
                                    <i>Chairman</i>
                                </p>
                                
                            </div>
                           <div class="container">
                            <a href="{{route('chairmandetail',['locale'=>$locale])}}" class="btn btn-primary btn-round">{{__('general.see-more')}}</a>
                           </div>
                        </div>
                        <div class="col-lg-6 text-center">
                            <br />
                            <img class="about-img" src="{{ asset('public/uploads/chairman/1600710031.png')}}" alt="">
                        </div>
                    
                    </div>
                    
                </div>
            </section>
            <section class="section section-about">
                <div class="container-fluid">
                    
                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Management Team
                        </div>
                    </div>

                    <div class="">
                    @php($i = 1)
                    <section class="section section-team">
                        <div class="container">
                            <div class="row justify-content-md-center">
                            @foreach( $manager as $row)
                                <div class="col-md-4 mt-3">
                            
                                    <div class="team-box flex center">
                                    
                                        <div class="team-thumb">
                                            <img src="{{ asset ($row->image)}}" alt="">
                                            <ul class="team-social flex center vcenter">
                                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                                            </ul>
                                        </div>
                                        <h4 class="team-name">{{ $row->title }}</h4>
                                        <p class="team-position">{{ $row->role }}</p>

                                    </div>
                            
                                </div>
                            @endforeach
                            </div>
                            

                            
                        </div>
                    </section>
                    </div>
                    
                </div>
            </section>

            
            <section class="section is-sm section-about">
                <div class="container">

                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Dynamic Staffs
                        </div>
                    </div>

                    <div>
                    <section class="section section-team">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                @foreach( $staff as $row)
                                <div class="col-lg-3 col-md-6 mt-4">
                                    <div class="team-box flex center">
                                        <div class="team-thumb">
                                            <img src="{{ asset ($row->image)}}" alt="">
                                            <ul class="team-social flex center vcenter">
                                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                                            </ul>
                                        </div>
                                        <h4 class="team-name">{{$row->title}}</h4>
                                        <p class="team-position">Position</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                    </div>

                </div>
            </section>

            <section class="section is-sm section-about">
                <div class="container">

                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Customer Support
                        </div>
                    </div>

                    <div>
                    <section class="section section-team">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                @foreach( $peoples as $row)
                                <div class="col-lg-3 col-md-6 mt-4">
                                    <div class="team-box flex center">
                                        <div class="team-thumb">
                                            <img src="{{ asset ($row->image)}}" alt="">
                                            <ul class="team-social flex center vcenter">
                                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                                            </ul>
                                        </div>
                                        <h4 class="team-name">{{$row->title}}</h4>
                                        <p class="team-position">Position</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                    </div>

                </div>
            </section>
            
        </div>
    </div>
    @endsection 