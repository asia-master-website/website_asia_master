@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-contact', 'active')

@section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  <script type="text/javascript">
    $(document).ready(function(){
      $("#submit-contact").submit(function(event){
        submit(event);
      })

      @if (Session::has('invalidData'))
        $("#form").show();
      @endif

    })
	@if(Session::has('msg'))
			toastr.success("{{ Session::get('msg') }}");
	@endif



    function submit(event){
      
	  name = $("#name").val();
      email = $("#email").val();
      phonenumber = $("#phonenumber").val();
      message = $("#message").val();
      recaptcha =$('#g-recaptcha-response').val();

      if(name != ''){
			
      if(validateEmail(email)){

        if(phone != ''){

        }else{
          toastr.error("Please enter your phone");
          event.preventDefault();
          $("#email").focus();
        }

      }else{
          toastr.error("Please give us correct email");
          event.preventDefault();
          $("#email").focus();
      }
              
      }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#fristname").focus();
      }
    }else{
          toastr.error("Please enter your name");
          event.preventDefault();
          $("#lastname").focus();
      }
    }
    
    function showApplicationForm(){
      form = $("#form");
      if(form.is(":visible")){
        form.hide();
      }else{
        form.show();
      }
    }

    
      function validatePhone(phone) {
          return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
      }

</script>
@endsection
@section ('contact')
@endsection

@section ('content')

    <div>
        <div class="mapouter">
            <div class="gmap_canvas">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d977.1341831500597!2d104.90012787914797!3d11.585031663269694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109512059fe45a9%3A0x6f6267adfbddd155!2sAsia%20Master%20(Cambodia)%20Co.%2C%20Ltd!5e0!3m2!1sen!2skh!4v1601740514109!5m2!1sen!2skh" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
               
            </div>
        </div>
    </div>

   
    <section class="section is-sm section-contact">

        <img class="section-particle top-0" src="https://digikit.netlify.app/assets/images/others/particle.svg" alt="">
        <div class="container">

            <div class="row ">

                <div class="col-lg-6">
                    <div class=" items-contact">
                        <div class="section-head">
                            <h2 class="section-title " style = " color: #1868b3 " >Contact Information</h2>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-item">
                                <h6>{{__('general.phone-number') }}</h6>
                                <a href="tel:+855 23 667 6666"><p class="contact-item-info"> + (855) 23 667 6666</p></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-item">
                                <h6>{{__('general.email-adress') }}</h6>
                                <p class="contact-item-info">info@asiamaster.net</p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-item">
                                <h6>{{__('general.local-adress') }}</h6>
                                <p class="contact-item-info">{{__('general.Address') }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                @if(Session::has('msg'))
                    <div class="pd-buttom">
                        <div class="alert alert-success" role="alert">
                        sent successful
                        </div>
                    </div>
                @endif
                <div class="col-lg-6">

                    <form class="contact-form" id="submit-contact" action="{{ route('submit-contact', $locale) }} " method="post">
                    {{ csrf_field() }} 
                    {{ method_field('PUT') }}
                        <div class="form-group">
                            <label>{{__('general.your-name') }}</label>
                            <input name="name"  placeholder="Name " class="form-control has-style1" id="name"
                                type="text">

                                @if ($errors->has('name'))
                                    <div class="pd-buttom">
                                        <div class="alert alert-danger" role="alert">
                                            {{ $errors->first('name') }}
                                        </div>
                                    </div>
                                @endif
                        </div>
                        <div class="form-group">
                            <label>{{__('general.your-phone-number') }}</label>
                            <input name="phone" placeholder="Phone Number " class="form-control has-style1" id="phone"
                                type="number">

                            @if ($errors->has('phone'))
                                <div class="pd-buttom">
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('phone') }}
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>{{__('general.Email') }}</label>
                            <input name="email" placeholder=" example@mail.com" class="form-control has-style1" id="email"
                                type="text">

                            @if ($errors->has('email'))
                                <div class="pd-buttom">
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('email') }}
                                    </div>
                                </div>
                            @endif

                        </div>
                        <label>{{__('general.tell-me-more-about-your-project') }}</label>
                        <textarea name="message" class="textarea has-style1" placeholder="How can we help?"></textarea>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-round" value="Submit">
                                <span> {{__('general.send-me') }} → </span>
                            </button>
                        </div>
                    </form>
                
                </div>

            </div>
        </div>
       
    </section>
  
@endsection