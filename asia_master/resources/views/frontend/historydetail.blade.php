@extends('frontend/layouts.master')

@section('title', 'chairman | Department of Good Govener')
@section('active-home', 'active')


@section ('appbottomjs')
@endsection
@section ('chairman')
@endsection

@section ('content')

<div class=" page-wrapper " >
    <div>
        <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/about-us.jpg') }}" alt="" style="width:100%" >
        <br />
    </div>
    <div class="container page-container"  >

         <section class="section section-about">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-12 page-section-title ">
                        History Conpany
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 text-center">
                        <br />
                        <br />
                        <img class="about-img" src="{{ asset('public/uploads/history/1600710046.png') }}" alt="">
                    </div>
                    <div class="col-lg-12 ceo-message">
                        <div>
                            <p class="" >
                                {!! $whoWeare->content ?? '' !!}
                            </p> 
                        </div>
                    </div>
                </div>
            </div>
        </section> 

        
    </div>
</div>


@endsection