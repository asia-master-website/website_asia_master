@extends('frontend/layouts.master')

@section('title', 'About | Asia Master (Cambodia) Co., LTD')
@section('active-service', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')

    <div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/service.png') }}" alt="" style="width:100%" >
            <br />
        </div>

        <div class="container page-container"  >
            @foreach( $services as $row)
           
                @if($row->is_position == 1)
               
                    <div class="row">
                        
                        <div class="col-lg-6 ceo-message" style="padding-left: 100px;" >
                            <h2 class="">{!! $row->title !!}</h2>
                            <p class="" >
                            {!! $row->content !!}<br>
                            </p>
                        </div>

                        <div class="col-lg-6 text-center">
                            <br />
                            <img class="about-img img-fluid" style="max-width:180px" src="{{ asset ($row->image)}}" alt="">
                        </div>

                        <div class="col-12">
                            <hr />
                        </div>
                        
                    </div>

                @else

                    <div class="row">
                        <div class="col-lg-6 text-center">
                            <br />
                            <img class="about-img img-fluid" style="max-width:180px" src="{{ asset ($row->image)}}" alt="">
                        </div>
                        <div class="col-lg-6 ceo-message">
                            <h2 class="">{!! $row->title !!}</h2>
                            <p class="" >
                            {!! $row->content !!}<br>
                            </p>
                        </div>

                        <div class="col-12">
                            <hr />
                        </div>
                        
                    </div>
                
                @endif

            @endforeach
        </div>
    </div>

 
@endsection