@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-about', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')
  
    {{-- <div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/about-us.jpg') }}" alt="" style="width:100%" >
            <br />
        </div>
        <div class="container page-container"  >

            <!-- <section class="section section-about">
                <div class="container-fluid">
                   
                    <div class="row">
                        <div class="col-12 page-section-title ">
                            CEO's Message
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 ceo-message">
                            <div>
                                <h2 class="">Welcome to Asia Master Cambodia!</h2>
                                <p class="" style="font-size: 18px">
                                <br />
                                &nbsp; &nbsp; <q>Asia Master Dear Fellow Colleagues, It has been more than ten years since the establishment of this company. Asia Master CAMBODIA was founded with long-range vision and dear-stated mission. As a founder, I am truly proud and appreciated to all management levels and fellow colleagues for the priceless, ongoing, physical and mental effort which has guaranteed this company's success and sustainability to date. Such outcome is definitely not a coincidence but a result of integrated factors such as our hard work, patience, honestly, loyalty, mutual understanding, unity so on and so forth. </q>
                                </p>
                            </div>

                            <div class="text-right" >
                                <p>
                                    <br />
                                    <b style="font-size: 20px" >Chheang Sopheak</b>
                                    <br />
                                    <i>Chairman</i>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 text-center">
                            <br />
                            <img class="about-img" src="{{ asset('public/uploads/chairman/1600710031.png')}}" alt="">
                        </div>
                    
                    </div>
                    
                </div>
            </section> -->

         
            <section class="section is-sm section-about">
                <div class="container">

                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Who We Are
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 text-center">
                            <br />
                            <br />
                            <img class="about-img" src="{{ asset('public/uploads/history/1600710046.png') }}" alt="">
                        </div>
                        <div class="col-lg-6 ceo-message">
                            <div>
                                <h2 class="">History</h2>
                                <p class="" >
                                    Asia Master Co., Ltd was established in July 07, 2009 with full license to provide all kinds of value added services for telecoms and call center service. It aims to become the leading contents provider and call center provider in Cambodia and commits to improve life enhancement and helping business in order to contribute to Cambodia’s economic growth. Asia Master in continuously expanding its services and commitments to reach its target of leading contents and call center provider to bring social benefits for Cambodian people.
                                </p>
                               
                            </div>

                            <div>
                                <hr />
                                <h2 class="">Mission</h2>
                                <p class="" >
                                    Our Mission is to bridge the gap of client’s mission to the customers’ expectation by providing helpful and resourceful human experience and solution with innovative technology to archive dedicated corporate customer care.
                                </p>
                            </div>

                            <div>
                                <hr />
                                <h2 class="">Vision</h2>
                                <p class="" >
                                    To be Asia’s first class partner contact center & customer care solution
                                </p>
                            </div>
                           
                        </div>
                       
                    
                    </div>

                </div>
            </section>
            
        </div>
    </div> --}}
    <div class=" page-wrapper " >
        <div>
            <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/about-us.jpg') }}" alt="" style="width:100%" >
            <br />
        </div>
        <div class="container page-container"  >

             <section class="section section-about">
                <div class="container-fluid">
                   
                    <div class="row">
                        <div class="col-12 page-section-title ">
                            Who We Are
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 text-center">
                            <br />
                            <br />
                            <img class="about-img" src="{!! asset ($whoWeare->image) ?? '' !!}" alt="">
                        </div>
                        <div class="col-lg-6 ceo-message">
                            <div>
                                <h2 class="">Asia Master (Cambodia) Co., LTD</h2>
                                <p class="" >
                                    {!! substr($whoWeare->content,0,880) ?? '' !!}
                                </p><br>
                                <a href="{{route('historydetail',['locale'=>$locale])}}" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal">{{__('general.see-more')}}</a>
                               
                            </div>

                            <div>
                                <hr />
                                <p class="" >
                                    {!! $missionAndVission->content ?? '' !!}
                                </p>
                            </div>     
                        </div>
                    </div>

                    <!-- Button trigger modal -->

                    
                </div>
            </section> 

            
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px;">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asia Master (Cambodia) Co., LTD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="" >
                    {!! ($whoWeare->content) ?? '' !!}
                </p><br>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
            </div>
            </div>
        </div>
    </div>
@endsection