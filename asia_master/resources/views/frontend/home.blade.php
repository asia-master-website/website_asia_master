@extends('frontend/layouts.master')

@section('title', 'Homepage | Asia Master (Cambodia) Co., LTD')
@section('active-home', 'active')

@section ('content')

    <!-- =========================== section feautures-->  
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @php($i = 1)
          @foreach( $slide as $row)
          <div class="carousel-item  @if($i++ == 1) active @endif" >
            <img src="{{ asset ($row->image)}}" class="d-block w-100" alt="">
          </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </a>
      </div>


    <section class="section is-sm section-feautures2">
        <div class="container">
            <div class="section-head d-flex justify-content-start align-items-center">
                <h2 class="section-title text-white mr-5">{{__('general.our-services') }}</h2>
                <p class="text-white max-30">We provide best services, and best quality.</p>
            </div>
            <div class="boxes">
                <div class="row min-30 flex center">
                    @foreach( $services as $row)
                        <div class="col-lg-3 col-md-6">
                        
                            <div class="box has-shadow">
                                <div class="box-particles">
                                <img src="https://digikit.netlify.app/assets/images/others/box-particles.svg" alt="">
                                </div>
                                <div class="box-num">
                                <a href="{{route('service',['locale'=>$locale])}}"><img class="m-auto service" data-aos="fade-left" data-aos-delay="700"
                                src="{{ asset ($row->image)}}" alt=""></a></div>
                                <h3 class="box-title">{{$row->title}}</h3>
                            </div>
                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- =========================== section About============================= -->
    <section class="section is-sm section-about my-about" >
         <img class="section-particle top-0" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt=""> 
        <div class="container">
            <div class="row flex vcenter">
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle ">Message</h5>
                        <h2 class="section-title ">Chairman Message</h2>
                        <p style="font-size: 20px;"class="section-desc">{!! substr($whatWeSay->content,0,800) ?? '' !!}...
                        </p><br>
                        <a href="{{route('chairmandetail',['locale'=>$locale])}}" class="btn btn-primary btn-round">{{__('general.see-more')}}</a>
                    </div>
                </div>
                <div style="margin-top: -19px;"class="col-lg-6">
                    <img class="about-img about-img1" src="{!! asset ($whatWeSay->image) ?? '' !!}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="section is-sm section-about " style="background-color:#FAF9FC;">
        <div class="container">
            <div class="row flex vcenter">
                <div class="col-lg-6">
                    <img class="about-img" src="{!! asset ($whoWeare->image) ?? '' !!}" alt="">
                </div>
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle ">Who We Are</h5>
                        <h2 class="section-title ">Asia Master (Cambodia) Co., LTD</h2>
                        <p class="section-desc">{!! substr($whoWeare->content,0,810) ?? '' !!}...</p>
                        <a href="{{route('historydetail',['locale'=>$locale])}}" class="btn btn-primary btn-round">{{__('general.see-more')}}</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <!-- =========================== section About-->
    <section class="section is-sm section-about">
    
        <div class="container">
            <div class="row flex vcenter">
                <div class="col-lg-6">
                    <div class="section-head">
                        <p class="section-desc">{!! $missionAndVission->content ?? '' !!}</p>
                        <hr />
                    </div>
                </div>
                <div class="col-lg-6">
                    <img class="about-img" src="{!! asset ($missionAndVission->image) ?? '' !!}" alt="">
                </div>
            </div>

            <div class="row">
                <div class="col-12" >
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </section>

    <!-- =========================== section Testimonials-->
    {{-- <section class="section is-sm section-testimonial section-grey  overflow-hidden ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                   
                    <div class="section-head mb-lg-0">
                        @foreach( $belief as $row)
                        <h5 class="section-subtitle"> {{__('general.Testimonials') }}</h5>
                        <h2 class="section-title">{{ $row->title }}<span class="text-primary"></span></h2>
                            </span>
                        </h3>
                        <p class=" section-desc  max-30 mt-1 mb-1">{{ $row->description }}</p>
                        <img class="stars" src="https://digikit.netlify.app/assets/images/others/stars.svg" alt="">
                        @endforeach
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    
                    <div class="client-wrap is-white">
                        @foreach( $customers as $row)
                        <div class="client-img">
                            <img src="{{ asset ($row->image)}}" alt="">
                        </div>
                        <p class="client-quote">{{ $row->description }}</p>
                        <div class="flex">
                            <strong class=" client-name ">{{ $row->title }}</strong>
                            <p class="client-position">CEO</p>
                        </div>
                        @endforeach
                    </div>
                   
                    <img class="section-shape2" src="https://digikit.netlify.app/assets/images/others/testimonials-white.svg" alt="">
                </div>
            </div>


        </div>
        <br />
        <br />
    </section> --}}
    <section class="section is-sm section-testimonial section-grey  overflow-hidden ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                   
                    <div class="section-head mb-lg-0">
                        <h5 class="section-subtitle"> {{__('general.Testimonials') }}</h5>
                        <h2 class="section-title">Our Belief<span class="text-primary"></span></h2>
                            </span>
                        </h3>
                        <p class=" section-desc  max-30 mt-1 mb-1">{!! $belief->content_part1 ?? '' !!}</p>
                        <img class="stars" src="https://digikit.netlify.app/assets/images/others/stars.svg" alt="">
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    
                    <div class="client-wrap is-white">
                        <div class="client-img">
                            <img src="{!! asset ($belief->image) ?? '' !!}" alt="">
                        </div>
                        <p class="client-quote">{!! $belief->content_part2 ?? '' !!}</p>
                        <div class="flex">
                            <strong class=" client-name ">Chea Sophalla</strong>
                            <p class="client-position">CEO</p>
                        </div>
                    </div>
                   
                    <img class="section-shape2" src="https://digikit.netlify.app/assets/images/others/testimonials-white.svg" alt="">
                </div>
            </div>


        </div>
        <br />
        <br />
    </section>

    <!-- ============================================| Customer & Partner -->
    <section class="section is-lg section-team">
        <div class="container">
            <div class="section-head">
               
                <h2 class="section-title is-center ">Customers & Partners</h2>
            </div>
            <div class="row min-30">
            @foreach( $partners as $row)
                <div class="col-lg-3 col-md-6">
                    <div class="team-box flex center">
                        <div class="team-thumb">
                            <img src="{{ asset ($row->image)}}" alt="">
                          
                        </div>
                       
                    </div>
                </div>      
             @endforeach
            </div>
        </div>
    </section>

 @endsection