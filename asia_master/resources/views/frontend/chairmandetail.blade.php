@extends('frontend/layouts.master')

@section('title', 'chairman | Department of Good Govener')
@section('active-home', 'active')


@section ('appbottomjs')
@endsection
@section ('chairman')
@endsection

@section ('content')
<div class=" page-wrapper " >
    <div>
        <img class="about-img" src="{{ asset('public/frontend/assets/images/banner/about-us.jpg') }}" alt="" style="width:100%" >
        <br />
    </div>
    <div class="container page-container"  >

         <section class="section section-about">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-12 page-section-title ">
                        CEO's Message
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 ceo-message">
                        <div>
                            <h2 class="">Welcome to Asia Master Cambodia!</h2>
                            <p class="">
                            <br />
                            &nbsp; &nbsp; {!! $whatWeSay->content ?? '' !!}
                            </p>
                        </div>

                        <div class="text-right" >
                            <p>
                                <br />
                                <b style="font-size: 20px" >Chheang Sopheak</b>
                                <br />
                                <i>Chairman</i>
                            </p>
                        </div>
                    </div>
                </div>
                
            </div>
        </section> 

        
    </div>
</div>

@endsection