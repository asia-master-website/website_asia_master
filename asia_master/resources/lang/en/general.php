<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
   

    'home'                                         => 'Home',
    'Page'                                         => 'Page',
    'about'                                        => 'About Us',
    'chairman-message'                             => 'Dear Fellow Colleagues, It has been more than ten years since the establishment of this company. Asia Master CAMBODIA was founded with long-range vision and dear-stated mission. As a founder, I am truly proud and appreciated to all management levels and fellow colleagues for the priceless, ongoing, physical and mental effort which has guaranteed this company success and sustainability to date. Such outcome is definitely not a coincidence but a result of integrated factors such as our hard work, patience, honestly, loyalty, mutual understanding, unity so on and so forth.',
    'asia-master'                                  =>'Asia Master Co., Ltd was established in July 07, 2009 with full license to provide all kinds of value added services for telecoms and call center service. It aims to become the leading contents provider and call center provider in Cambodia and commits to improve life enhancement and helping business in order to contribute to Cambodia’s economic growth. Asia Master in continuously expanding its services and commitments to reach its target of leading contents and call center provider to bring social benefits for Cambodian people.',
    'vision'                                       =>'To be Asia’s first class partner contact center & customer care solution.',
    'mission'                                      =>'Our Mission is to bridge the gap of client’s mission to the customers’ expectation by providing helpful and resourceful human experience and solution with innovative technology to archive dedicated corporate customer care.',
    '24-7day'                                      =>'24h/7days Multilingual Inbound Call Center',
    '+Responding to enquiries'                     =>'+Responding to enquiries',
    '+Providing technical support'                 =>'+Providing technical support',
    '+Collecting customer’s feedback'              =>'+Collecting customer’s feedback',
    '+Handling the customer complaints'            =>'+Handling the customer complaints',
    '+Other telephone answering services'          =>'+Other telephone answering services',
    'multilingual-outbound-call-center'            =>'Multilingual Outbound Call Center',
    '+Customer Acquisition'                        =>'+Customer Acquisition',
  '+Customer Retention'                            =>'+Customer Retention',
   'tele-customer-care-system'                     =>'Tele Customer Care System',
   '+Customer care software development'           =>'+Customer care software development',
   '+Call center setup '                           =>'+Call center setup ',
   '+Call center system consulting'                =>'+Call center system consulting',
   '+Training of call center agents'               =>'+Training of call center agents',
   'customer-service-solution'                     =>'Customer Service Solution',
   '+Outsource showroom management'                =>'+Outsource showroom management',
   '+Sustainable reception management'             =>'+Sustainable reception management',
   '+Showroom/Front desk/ Receptionist Staffing'   =>'+Showroom/Front desk/ Receptionist Staffing',
   '+Customer service SOP development'             =>'+Customer service SOP development',
   '+Training of customer service agent'           =>'+Training of customer service agent',
   'our-people'                                    =>'Our People',
   'Together we provide you with quality and efficiency'=>'Together we provide you with quality and efficiency',
   'our-services'                                 =>'Our Services',
   'our-partners'                                 =>'Our Partners',
   'career-opportunity'                           =>'Career Opportunity',
   'Contact-us'                                   =>'Contact Us',
   'What he say'                                  =>'What he say',
   'Chairman is Message'                          =>'Chairman Message',
   'see-more'                                     =>'See more',
   'History'                                      =>'History',
   'What We Do Best'                              =>'what are our mission and vision?',
   'Our Vision'                                   =>'Our Vision',
   'Our Mission'                                  =>'Our Mission',
   'We provide best services, and best quality.'  =>'We provide best services, and best quality.',
   'Share'                                        =>'Asia Master strong talent management practices sustain employee retention, enabling us to build flexible and creative staffing models for greater efficiency and enhanced service levels',
   'we are one'                                   =>'Our Customer and',
   'We understand'                                =>' We understand the importance of approaching each work integrally and believe in the power ofsimpleand easy communication.',
   'Get in touch with us'                         =>'Get in touch with us',
   'Please submit'                                =>'Please submit your information will follow up with you within 24 hours.',
   'Address'                                      =>'Address: 153 AB Street 289, boeung kak 1, toul kork district, Phnom Penh, Cambodia',
   'Testimonials'                                 =>'Testimonials',
   'Our Belief'                                   =>'Our Belief',
   'Nothing is more important'                    =>'Nothing is more important than your customers, for without them you would not be in business. At ASIA MASTER, we fully understand the important and magnitude of being entrusted to support your customers.',
   'Company'                                      =>'Company',
   'Useful links'                                 =>'Useful links',
   'Asia Master Cambodia'                         =>'Asia Master Cambodia',
   'Contact Form'                                 =>'Contact Form',
   'tell-me-more-about-your-project'              =>'Tell me more about your project',
   'your-name'                                    =>'Your Name',
   'Email'                                        =>'Email',
   'send-me'                                      =>'Send me',
   'how-can-i-help-you?'                          =>'How can I help you?',
   'are-you-in-a-hurry?'                          =>'Are you in a hurry?',
   //////////////////////////////footer
   'contact-us-now'                               =>'Contact us now',
   'phone'                                        =>'Phone',
   'email'                                        =>'Email',
   'Addresss'                                     =>'Address',
   'enter-your-email-to-contact us'               =>'Enter your email to contact us',
   'Send'                                         =>'Send',
   'your-phone-number'                            =>'Your Phone Number',
   'phone-number'                                 =>'Phone Number',
   'email-adress'                                 =>'Email Adress',
   'local-adress'                                 =>'Local Adress',
   'key-working-groups'                           =>'Key working groups',
   'President'                                    =>'President',
   'Management'                                   =>'Management',
   'working-hours'                                =>'Working hours',
   '24 Hour / 7 Days'                             =>'24 Hour / 7 Days',
   'monday-sunday'                                =>'Monday - Sunday',
    
   
];