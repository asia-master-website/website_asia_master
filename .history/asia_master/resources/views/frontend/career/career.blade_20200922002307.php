@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-career', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')
 

  <!-- =========================== header-->
  <header class="header-page header-page5">
    <div class="header-wrap">
        <div class="container">
        <h2 class="header-title" data-aos="fade-up" data-aos-delay="500">{{__('general.Career Opportunity')}}</h2>
        <img class="shape" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt="">
    </div>
  </div>
  </header>
<!-- ================================= Section plans -->
<section class="section is-sm section-blog">
    <div class="container">
        <div class="section-head">
            <h2 class="section-title is-center ">{{__('general.Career Opportunity')}}<span class="text-primary">. </span>
            </h2>
            <p class="section-desc is-center  ml-auto mr-auto mt-20">
                {{__('general.We understand') }}</p>
        </div>
        <div class="row">
            @foreach( $care as $row)
                <div class="col-md-4">
                    <div class="">
                        <div class="post-wrap mt-0">
                        <a href="{{ route('career-detail', ['locale' => $locale, 'id'=>$row->id]) }}">
                                <div class="post-img">
                                    <img src="{{ asset ($row->image)}}" alt="">
                                </div>
                            </a>
                            <div class="post-content mb-0">
                                <!-- <div class="post-meta">
                                    <div class="post-tag"> <span class="tag-item">Tech</span> </div>
                                </div> -->
                                <h3 class="post-title"><a href="#">{{ $row->title }}</a>
                                </h3>

                                <!-- <span class="post-author">date <a href="#" rel="author">Albert Kerri</a></span> -->
                                <span class="post-date"> 02 September 2019 </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach 
        </div>
    </div>
</section>
 
@endsection