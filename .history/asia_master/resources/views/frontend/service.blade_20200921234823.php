@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-about', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')

<header class="header-page header-page3">
    <div class="header-wrap">
        <div class="container">
            <h2 class="header-title" data-aos="fade-up" data-aos-delay="500">{{__('general.Our Services')}}</h2>
            <img class="shape" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt="">
        </div>
    </div>
</header>
<!-- =========================== section About-->
<section class="section is-sm section-about">
    <div class="container">
        @php($i = 1)
        @foreach( $services as $row)
        <div class="row flex vcenter ">
            @if($row->is_position == 1)
                <div class="col-lg-6 text-center">
                <img class="" style="width:300px;" class="about-img" src="{{ asset ($row->image)}}" alt="">
                </div>
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle "></h5>
                        <h2 class="section-title ">{!! $row->title !!}</h2>
                        <p class="section-desc"> 
                            {!! $row->content !!}<br>
                        </p>
                       
                    </div>
                </div>
            @else
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle "></h5>
                        <h2 class="section-title ">{!! $row->title !!}</h2>
                        <p class="section-desc">
                            {!! $row->content !!}<br>
                        </p>
                       
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    <img class="" style="width:300px;" class="about-img" src="{{ asset ($row->image)}}" alt="">
                </div>
            @endif
        </div>
        @endforeach
    </div>
</section>
 
@endsection