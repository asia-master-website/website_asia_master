@extends('frontend/layouts.master')

@section('title', 'chairman | Department of Good Govener')
@section('active-history', 'active')


@section ('appbottomjs')
@endsection
@section ('chairman')
@endsection

@section ('content')
<header class="header-page header-page4">
    <div class="header-wrap">
        <div class="container">
            <h2 class="header-title" data-aos="fade-up" data-aos-delay="500">{{__('History Detail')}}</h2>
            <img class="shape" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt="">
        </div>
    </div>
</header>
<!-- =========================== section About-->
<section class="section is-sm section-about " style="background-color:#FAF9FC;"​>
    <div class="container">
        <div class="row flex vcenter">
            @foreach( $history as $row)
            <div class="col-lg-6">
                <div class="section-head">
                    <h5 class="section-subtitle ">{{__('general.History')}}</h5>
                    <h2 class="section-title ">{{ $row->title }}</h2>
                    <p class="section-desc">{{ $row->description }}</p>
                   
                </div>
            </div>
            <div class="col-lg-6">
                <img class="about-img" src="{{ asset ($row->image)}}" alt="">
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection