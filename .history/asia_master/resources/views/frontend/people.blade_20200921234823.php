@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-about', 'active')


@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')


<body>
    <!-- =========================== header-->
    <header class="header-page header-page2">
            <div class="header-wrap">
                <div class="container">
                <h2 class="header-title" data-aos="fade-up" data-aos-delay="500">{{__('general.Our People')}}</h2>
                <img class="shape" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt="">
            </div>
        </div>
    </header>
    <!-- ================================= Section plans -->
    <!-- =========================== section team-->
    <section class="section is-lg section-team">
        <div class="container">
            <div class="section-head">
                <h2 class="section-title is-center ">{{__('general.Management')}}</h2>
            </div>
            <div class="row min-30 justify-content-md-center">
            @foreach( $manager as $row)
                <div class="col-lg-3 col-md-6">
               
                    <div class="team-box flex center">
                    
                        <div class="team-thumb">
                            <img src="{{ asset ($row->image)}}" alt="">
                            <ul class="team-social flex center vcenter">
                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                            </ul>
                        </div>
                        <h4 class="team-name">{{ $row->title }}</h4>
                        <p class="team-position">{{ $row->role }}</p>

                    </div>
               
                </div>
            @endforeach
            </div>
        </div>
        <div class="container">
            <div class="section-head">
                <h2 class="section-title is-center ">{{__('general.President')}}</h2>
            </div>
            <div class="row min-30 justify-content-md-center">
                @foreach( $peoples as $row)
                <div class="col-lg-3 col-md-6">
               
                    <div class="team-box flex center">
                    
                        <div class="team-thumb">
                            <img src="{{ asset ($row->image)}}" alt="">
                            <ul class="team-social flex center vcenter">
                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                            </ul>
                        </div>
                        <h4 class="team-name">{{ $row->title }}</h4>
                        <p class="team-position">{{ $row->role }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="container">
            <div class="section-head">
                <h2 class="section-title is-center ">{{__('general.Key working groups')}}</h2>
            </div>
            <div class="row min-30 justify-content-md-center">
                @foreach( $staff as $row)
                <div class="col-lg-3 col-md-6">
                    <div class="team-box flex center">
                        <div class="team-thumb">
                            <img src="{{ asset ($row->image)}}" alt="">
                            <ul class="team-social flex center vcenter">
                                <li> <a href="#"> <i class="fab fa-twitter"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-facebook-f"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-quora"></i></a> </li>
                                <li> <a href="#"> <i class="fab fa-reddit"></i></a> </li>
                            </ul>
                        </div>
                        <h4 class="team-name">{{$row->title}}</h4>
                        <p class="team-position">{{$row->description}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endsection