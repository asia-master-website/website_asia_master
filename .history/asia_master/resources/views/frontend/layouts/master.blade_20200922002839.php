<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from digikit.netlify.app/pages/home_3 by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 25 Aug 2020 03:53:42 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset ('public/frontend/assets/images/favicon.png')}}" rel="shortcut icon">
    <link rel="shortcut icon" href="{{ asset('public/frontend/assets/img/logos/aa.html') }}" type="image/x-icon">
    <!-- ========================================= Css files -->
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/owl.theme.default.min.html') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/assets/css/style.css') }}">
    <link href="{{ asset ('public/frontend/css/owl.theme.css') }}" rel="stylesheet"> 
    <title>Asia Master Cambodia</title>
</head>

<body>
    <!-- =========================== header-->
    <header class="header has-style2 section">
        <img class="section-particle top-0" src="https://digikit.netlify.app/assets/images/others/particle.svg" alt="">

        <!-- =========================== navbar-->
        <nav class="navbar is-dark">
            <div class="container">
                <div class="flex">
                    <a href="{{route('home',['locale'=>$locale])}}" class="navbar-brand flex vcenter"><img data-aos="fade-right" class="logo"
                            src="{{ asset('public/frontend/assets/img/logo1.png') }}" style="width: 165px; height: auto;"></a>
                    <ul class="navbar-menu">
                        
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-about')"
                            href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-people')"
                            href="{{route('people',['locale'=>$locale])}}">{{__('general.Our People')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-service')"
                            href="{{route('service',['locale'=>$locale])}}">{{__('general.Our Services')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-partner')"
                            href="{{route('partner',['locale'=>$locale])}}">{{__('general.Our Partners')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-career')"
                            href="{{route('career',['locale'=>$locale])}}">{{__('general.Career Opportunity')}}</a>
                        </li>
                        <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page @yield('active-contact')"
                        href="{{route('contact',['locale'=>$locale])}}">{{__('general.Contact Us')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="level-right">
                    <!-- your list menu here -->
                    <div class="navbar-menu">
                        <!-- <a href="#" class="btn text-primary "> Sign in
                        </a> -->
                        <a> 
                        <a href="{{route($defaultData['routeName'], $defaultData['khRouteParamenters'])}}"> <img src="{{ asset('public/frontend/assets/img/logo/bng.png') }}" alt=""></a>
                        <a href="{{route($defaultData['routeName'], $defaultData['enRouteParamenters'])}}"> <img src="{{ asset('public/frontend/assets/img/logo/hgf.png') }}" alt=""></a>
                            
                        </a>
                    </div>
                    <div class="mobile-menu">
                        <!-- your list menu in mobile here -->
                        <ul>
                            <li><a class="fade-page" href="home_1.html">Home 1</a>
                            </li>
                            <li><a class="fade-page" href="home_2.html">Home 2</a>
                            </li>
                            <li><a class="fade-page" href="home_3.html">Home 3</a>
                            </li>
                            <li><a class="fade-page" href="home_4.html">Home 4</a>
                            </li>
                            <li><a class="fade-page" href="home_5.html">Home 5</a>
                            </li>
                            <li><a class="fade-page" href="home_1_grad.html">Home 1 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_2_grad.html">Home 2 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_3_grad.html">Home 3 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_4_grad.html">Home 4 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                            <li><a class="fade-page" href="home_5_grad.html">Home 5 <span
                                        class="text-grad">Gradient</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="flex">
                        <div class="menu-toggle-icon">
                            <div class="menu-toggle">
                                <div class="menu">
                                    <input type="checkbox">
                                    <div class="line-menu"></div>
                                    <div class="line-menu"></div>
                                    <div class="line-menu"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
     
    </header>

    @yield('content')
            

 <!--==============================================================> footer -->
 <footer class="footer has-style1">
        <div class="footer-head">
            <img class="rounded sm-hidden " src="https://digikit.netlify.app/assets/images/others/hero-round.svg" alt="">
            <div class="overflow-hidden">
                <!-- <img class="footer-shape" src="https://digikit.netlify.app/assets/images/others/footer-shape.svg" alt=""> -->
            </div>
            <button id="button" class="to-top">
            <img class="footer-icon-style" src="{{ asset('public/frontend/assets/img/logofooter.png') }}" alt="">
            </button>
            <div class="section is-sm ">
                <div class="container">
                    <h2 class="section-title is-center text-white">{{__('general.contact-us-now') }}</h2>
                    <div class="flex center mt-5">
                        <form class=" bg-transparent ">
                            <input type="text" class="footer-input" placeholder="{{__('general.Enter your email to contact us') }}">
                            <li href="{{route('contact',['locale'=>$locale])}}"><button class="btn btn-primary btn-round footer-btn">{{__('general.Send') }} </button></li>
                        </form>
                    </div>
                    <div class="row footer-contact min-30">
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-item">
                                <h6>{{__('general.phone') }}</h6>
                                <p class="contact-item-info">(+855) 23 667 6666</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-item">
                                <h6>{{__('general.email') }}</h6>
                                <p class="contact-item-info">info@asiamaster.net</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-item">
                                <h6>{{__('general.Addresss') }}</h6>
                                <p class="contact-item-info">{{__('general.Address') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>
 
 <footer class="footer has-style2">
    <div class="footer-body">
        <div class="container">
            <div class="row ">
                <!-- <div class="col-lg-3">
                    <div class="footer-desc">
                        <div class="logo">
                            <img src="{{ asset('public/frontend/assets/img/logofooter.png') }}" alt="">
                        </div>
                        <p>{{__('general.Address') }}</p>
                    </div>
                </div> -->
                <div class="col-lg-5 col-6">
                    <h6 class="list-title">{{__('general.Our Services')}}</h6>
                    <ul class="list-items">
                        <li> <a href="#">*{{__('general.24-7day')}}</a> </li>
                        <li> <a href="#">*{{__('general.Multilingual Outbound Call Center')}}</a> </li>
                        <li> <a href="#">*{{__('general.Tele Customer Care System')}}</a> </li>
                        <li> <a href="#">*{{__('general.Customer Service Solution')}}</a> </li>
                    </ul>
                </div>
                <div class="col-lg-3  col-sm-4 col-6">
                    <h6 class="list-title">{{__('general.Page') }}</h6>
                    <ul class="list-items">
                        <li> <a  href="{{route('partner',['locale'=>$locale])}}">{{__('general.Our Partners')}}</a> </li>
                        <li class="nav-item @yield('active-about')"> <a class="nav-link"
                            href="{{route('about',['locale'=>$locale])}}">{{__('general.about')}}</a> 
                         </li>
                         <li class="nav-item @yield('active-people')"> <a class="nav-link"
                            href="{{route('people',['locale'=>$locale])}}">{{__('general.Our People')}}</a>
                         </li>
                         <li data-aos="fade-left" data-aos-delay="100"> <a class="fade-page"
                            href="{{route('service',['locale'=>$locale])}}">{{__('general.Our Services')}}</a>
                        </li>
                         </li>
                        <li> <a href="#">{{__('general.Contact Us')}}</a> </li>
                    </ul>
                </div>
                <div class="col-lg-2  col-sm-4 col-6">
                    <h6 class="list-title">{{__('general.Working hours') }}</h6>
                    <ul class="list-items">
                        <li> <a href="#">24 Hour / 7 Days</a> </li>
                        <li> <a href="#">Monday - Sunday</a> </li>
                        <!-- <li> <a href="#">Contact us</a> </li> -->
                    </ul>
                </div>
                <!-- <div class="col-lg-2  col-sm-4 col-6">
                    <h6 class="list-title">Legal</h6>
                    <ul class="list-items">
                        <li> <a href="#">Terms</a> </li>
                        <li> <a href="#">Privacy </a> </li>
                        <li> <a href="#">Cookies</a> </li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p class="copyright text-center text-copyright">© 2020 Asia Master Cambodia Co.,Ltd. All Right Reserved</p>
    </div>
</footer>

<!-- ====================================== js files  -->
<script src="{{ asset('public/frontend/assets/js/plugins/jQuery.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/modernizr.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/feather-icons.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/slick.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/owl.carousel.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/aos.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/typed.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/all.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/plugins/jquery.counterup.min.js') }}"></script>
<script src="../../unpkg.com/ionicons%404.5.10-0/dist/ionicons.js') }}"></script>
<script src="{{ asset('public/frontend/assets/js/main.js') }}"></script>
</body>
</html>