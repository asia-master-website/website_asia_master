@extends('frontend/layouts.master')

@section('title', 'About | Department of Good Govener')
@section('active-home', 'active')


@section ('appbottomjs')
@endsection
@section ('about')
@endsection

@section ('content')

    <!-- =========================== section feautures-->
    <div class="container-fluid">
        
        <div class="header-wrap">
            <div class="row align-items-center">
                <!-- <div class="col-lg-6">
                    <h2 class="header-title" data-aos="fade-up" data-aos-delay="500">Our Belief</h2>
                    <p class="header-desc" data-aos="fade-up" data-aos-delay="600">
                    Nothing is more important than your customers, for without them you
                    would not be in business. At ASIA MASTER, we fully understand the important
                    and magnitude of being entrusted to support your customers.
                    </p>
                    <a href="#" class="btn btn-primary btn-round" data-aos="fade-up" data-aos-delay="700">Let's Get
                        Started<ion-icon name="arrow-forward"></ion-icon></a>
                </div> -->
                <div class="col-lg-12">
                    @foreach( $slide as $row)
                    <div class="header-img">
                        <img class="w-100" data-aos="fade-left" data-aos-delay="700"
                            src="{{ asset ($row->image)}}" alt="">
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    <div class="hero-particles particle-container">
        <div class="particle particle-6"></div>
    </div>
  <section class="section is-sm section-feautures2">
        <div class="container">
            <div class="section-head d-flex justify-content-start align-items-center">
                <h2 class="section-title text-white mr-5">{{__('general.Our Services') }}</h2>
                <p class="text-white max-30">{{__('general.We provide best services, and best quility.') }}</p>
            </div>
            <div class="boxes">
                <div class="row min-30 flex center">
                    @foreach( $services as $row)
                    <div class="col-lg-3 col-md-6">
                       
                        <div class="box has-shadow">
                            <div class="box-particles">
                                <img src="https://digikit.netlify.app/assets/images/others/box-particles.svg" alt="">
                            </div>
                            <div class="box-num"><img class="m-auto service" data-aos="fade-left" data-aos-delay="700"
                            src="{{ asset ($row->image)}}" alt=""></div>
                            <h3 class="box-title">{{$row->title}}</h3>
                        </div>
                    </div>
                         @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- =========================== section About-->
    <section class="section is-sm section-about my-about" >
    <img class="section-particle top-0" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt="">
        <div class="container">
            <div class="row flex vcenter">
                @foreach( $chairman as $row)
                <div class="col-lg-6">
                    <img class="about-img about-img1" src="{{ asset ($row->image)}}" alt="">
                </div>
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle "> {{__('general.What he say')}}</h5>
                        <h2 class="section-title ">{{ $row->title }}</h2>
                        <p class="section-desc">{{ $row->description }}</p>
                        <a href="{{route('chairmandetail',['locale'=>$locale])}}" class="btn btn-primary btn-round">{{__('general.See more')}}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- =========================== section About-->
     <section class="section is-sm section-about " style="background-color:#FAF9FC;">
        <div class="container">
            <div class="row flex vcenter">
                @foreach( $history as $row)
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle ">{{__('general.History')}}</h5>
                        <h2 class="section-title ">{{ $row->title }}</h2>
                        <p class="section-desc">{{ $row->description }}</p>
                        <a href="{{route('historydetail',['locale'=>$locale])}}" class="btn btn-primary btn-round">{{__('general.See more')}}</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img class="about-img" src="{{ asset ($row->image)}}" alt="">
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- =========================== section About-->
    <section class="section is-sm section-about">
    {{-- <img class="section-particle2 top-0" src="{{ asset('public/frontend/assets/images/partern1.png') }}" alt=""> --}}
        <div class="container">
            <div class="row flex vcenter">
                <div class="col-lg-6">
                    @foreach( $mission as $row)
                    <img class="about-img" src="{{ asset ($row->image)}}" alt="">
                    @endforeach
                </div>
                <div class="col-lg-6">
                    <div class="section-head">
                        <h5 class="section-subtitle ">{{__('general.What We Do Best')}}</h5>
                        @foreach( $vision as $row)
                        <h2 class="section-title ">{{ $row->title }}</h2>
                        <p class="section-desc">{{ $row->description }}</p>
                        @endforeach
                        @foreach( $mission as $row)
                        <h2 class="section-title ">{{ $row->title }}</h2>
                        <p class="section-desc">{{ $row->description }}</p>
                        @endforeach
                        <!-- <a href="#" class="btn btn-primary btn-round">See our stadies</a> -->
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- =========================== section Testimonials-->
    <section class="section is-sm section-testimonial section-grey  overflow-hidden ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                   
                    <div class="section-head mb-lg-0">
                        @foreach( $belief as $row)
                        <h5 class="section-subtitle"> {{__('general.Testimonials') }}</h5>
                        <h2 class="section-title">{{ $row->title }}<span class="text-primary"></span></h2>
                            </span>
                        </h3>
                        <p class=" section-desc  max-30 mt-1 mb-1">{{ $row->description }}</p>
                        <img class="stars" src="https://digikit.netlify.app/assets/images/others/stars.svg" alt="">
                        @endforeach
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    
                    <div class="client-wrap is-white">
                        @foreach( $customers as $row)
                        <div class="client-img">
                            <img src="{{ asset ($row->image)}}" alt="">
                        </div>
                        <p class="client-quote">{{ $row->description }}</p>
                        <div class="flex">
                            <strong class=" client-name ">{{ $row->title }}</strong>
                            <p class="client-position">CEO</p>
                        </div>
                        @endforeach
                    </div>
                   
                    <img class="section-shape2" src="https://digikit.netlify.app/assets/images/others/testimonials-white.svg" alt="">
                </div>
            </div>


        </div>
    </section>
       <section class="section is-lg section-team">
        <div class="container">
            <div class="section-head">
                <h5 class="section-subtitle is-center"> {{__('general.we are one') }} </h5>
                <h2 class="section-title is-center ">{{__('general.Our Partners') }}</h2>
            </div>
            <div class="row min-30">
            @foreach( $partners as $row)
                <div class="col-lg-3 col-md-6">
                    <div class="team-box flex center">
                        <div class="team-thumb">
                            <img src="{{ asset ($row->image)}}" alt="">
                            <ul class="team-social flex center vcenter">
                                <li> <a href="#"> <svg class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg><!-- <i class="fab fa-twitter"></i> --></a> </li>
                                <li> <a href="#"> <svg class="svg-inline--fa fa-facebook-f fa-w-10" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg><!-- <i class="fab fa-facebook-f"></i> --></a> </li>
                                <li> <a href="#"> <svg class="svg-inline--fa fa-quora fa-w-14" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="quora" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M440.5 386.7h-29.3c-1.5 13.5-10.5 30.8-33 30.8-20.5 0-35.3-14.2-49.5-35.8 44.2-34.2 74.7-87.5 74.7-153C403.5 111.2 306.8 32 205 32 105.3 32 7.3 111.7 7.3 228.7c0 134.1 131.3 221.6 249 189C276 451.3 302 480 351.5 480c81.8 0 90.8-75.3 89-93.3zM297 329.2C277.5 300 253.3 277 205.5 277c-30.5 0-54.3 10-69 22.8l12.2 24.3c6.2-3 13-4 19.8-4 35.5 0 53.7 30.8 69.2 61.3-10 3-20.7 4.2-32.7 4.2-75 0-107.5-53-107.5-156.7C97.5 124.5 130 71 205 71c76.2 0 108.7 53.5 108.7 157.7.1 41.8-5.4 75.6-16.7 100.5z"></path></svg><!-- <i class="fab fa-quora"></i> --></a> </li>
                                <li> <a href="#"> <svg class="svg-inline--fa fa-reddit fa-w-16" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="reddit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M201.5 305.5c-13.8 0-24.9-11.1-24.9-24.6 0-13.8 11.1-24.9 24.9-24.9 13.6 0 24.6 11.1 24.6 24.9 0 13.6-11.1 24.6-24.6 24.6zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-132.3-41.2c-9.4 0-17.7 3.9-23.8 10-22.4-15.5-52.6-25.5-86.1-26.6l17.4-78.3 55.4 12.5c0 13.6 11.1 24.6 24.6 24.6 13.8 0 24.9-11.3 24.9-24.9s-11.1-24.9-24.9-24.9c-9.7 0-18 5.8-22.1 13.8l-61.2-13.6c-3-.8-6.1 1.4-6.9 4.4l-19.1 86.4c-33.2 1.4-63.1 11.3-85.5 26.8-6.1-6.4-14.7-10.2-24.1-10.2-34.9 0-46.3 46.9-14.4 62.8-1.1 5-1.7 10.2-1.7 15.5 0 52.6 59.2 95.2 132 95.2 73.1 0 132.3-42.6 132.3-95.2 0-5.3-.6-10.8-1.9-15.8 31.3-16 19.8-62.5-14.9-62.5zM302.8 331c-18.2 18.2-76.1 17.9-93.6 0-2.2-2.2-6.1-2.2-8.3 0-2.5 2.5-2.5 6.4 0 8.6 22.8 22.8 87.3 22.8 110.2 0 2.5-2.2 2.5-6.1 0-8.6-2.2-2.2-6.1-2.2-8.3 0zm7.7-75c-13.6 0-24.6 11.1-24.6 24.9 0 13.6 11.1 24.6 24.6 24.6 13.8 0 24.9-11.1 24.9-24.6 0-13.8-11-24.9-24.9-24.9z"></path></svg><!-- <i class="fab fa-reddit"></i> --></a> </li>
                            </ul>
                        </div>
                        <!-- <h4 class="team-name">amira yerden</h4>
                        <p class="team-position">developper</p> -->

                    </div>
                </div>      
             @endforeach
            </div>
        </div>
    </section>
    @endsection