<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
  
use App\Model\Services;
class ServiceController extends FrontendController
{
    
    public function index($locale = "en") {
       // $slideservice = Slideservice::select('id','title','image','description')->where('is_published', 1)->get();
       $services = Services::select($locale.'_title as title','image',$locale.'_description as description')->where('is_published',1)->get();
        $defaultData = $this->defaultData($locale);
        return view('frontend.service',
        ['locale'=> $locale,
        'services'=>$services,
         'defaultData' => $defaultData]);
  
 }
}